import Login from "./user/Login";
import Register from "./user/Register";

import ViewCreateProduct from "./admin/CreateProduct";
import ViewAllProducts from "./admin/AllProducts";

export default [
    Login,
    Register,

    ViewCreateProduct,
    ViewAllProducts
];
