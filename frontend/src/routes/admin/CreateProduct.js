import ViewCreateProduct from '../../components/AdminSettings/Products/ViewCreateProduct';

export default {
   exact: true,
   name: 'Create Product',
   // role: 'sellerUser',
   protected: true,
   path: '/product',
   component: ViewCreateProduct,
};



