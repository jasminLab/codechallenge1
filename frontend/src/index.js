import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import { transitions, positions, types, Provider as AlertProvider } from 'react-alert';
import AlertTemplate from "react-alert-template-basic";
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import store from './store';

// optional configuration
const options = {
   // you can also just use 'bottom center'
   position: positions.TOP_CENTER,
   //  type: types.SUCCESS,
   timeout: 8000,
   offset: '90px',
   // you can also just use 'scale'
   transition: transitions.SCALE
}

ReactDOM.render(
   <AlertProvider template={AlertTemplate} {...options}>
      <Provider store={store}>
         <React.StrictMode>
            <App />
         </React.StrictMode>
      </Provider>
   </AlertProvider>,
   document.getElementById('root')
);

serviceWorker.unregister();
