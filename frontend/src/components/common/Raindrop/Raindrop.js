import React from 'react';
import './Raindrop.scss';

const RaindropImage = ({ children }) => (
   <div>
      <div className="container image-farm">
         <p className="raindrop-title">
            BK TecHouse Challenge
         </p>
         <p className="raindrop-title2">
            Founded in 2016, BK TecHouse is an Innovative Technology Company dedicated to delighting our customers, 
            employees and shareholders by providing high quality Innovative Technology products and services that empower our customers to strive in a fast changing market. 
            BK TecHouse is the perfect partner in the digital age when Big no longer beats Small, and it is the Fast beating the Slow! Our Key Differentiators are our Passionate and Professional Staff, 
            our Customer-Centric and Lean-Agile approach to solution development, our Cores Values and Servant Leadership Culture. 
            BKTecHouse is the Engine Powering Innovation!
         </p>
      </div>
      {children}
   </div>
);

export default RaindropImage;
