import React, { Component } from "react";
import { Icon, Menu, Segment, Sidebar, Accordion } from "semantic-ui-react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import "./Sidebar.scss";

class UserSidebar extends Component {
    state = {
        activeIndex: 1,
    };

    handleClick = (e, titleProps) => {
        const { index } = titleProps;
        const { activeIndex } = this.state;
        const newIndex = activeIndex === index ? -1 : index;

        this.setState({
            activeIndex: newIndex
        });
    };

    componentDidMount = () => {
    };

    static getDerivedStateFromProps = (props) => {
    };

    render() {
        const { isAuth, userType } = this.props;
        const { activeIndex } = this.state;

        return (
            <div className=" Sidebar-content">
                <div className="sidebar-height">
                    <Sidebar.Pushable>
                        <Sidebar
                            className="sidebar-width"
                            as={Menu}
                            animation="overlay"
                            icon="labeled"
                            vertical
                            visible>
                            {!isAuth ? (
                                ""
                            ) : (
                                <Menu.Item className="padding-side">
                                    <Icon
                                        size="mini"
                                        className="icons-style"
                                        name="dashboard"
                                    />
                                    <Link className="link-color" to="/dashboard">
                                        Dashboard
                                    </Link>
                                </Menu.Item>
                            )}

                            {isAuth && (userType === 'sellerUser' || userType === 'adminUser') ? (
                                <Menu.Item>
                                    <Icon name="setting" />
                                    <Accordion>
                                        <Accordion.Title
                                            active={activeIndex === 6}
                                            index={6}
                                            onClick={this.handleClick}>
                                            <div>
                                                Settings
                                                <Icon
                                                    className="icon-accordeon"
                                                    name="dropdown"
                                                />
                                            </div>
                                        </Accordion.Title>
                                        <Accordion.Content active={activeIndex === 6}>
                                            <Link className="link-color" to="/product">
                                                <p className="text-side">Products</p>
                                            </Link>
                                        </Accordion.Content>
                                    </Accordion>
                                </Menu.Item>
                            ) : (
                                ""
                            )}
                        </Sidebar>
                        <Sidebar.Pusher>
                            <Segment basic />
                        </Sidebar.Pusher>
                    </Sidebar.Pushable>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({
    user: {
        isAuth,
        profile: { userType, name }
    }
}) => ({
    isAuth,
    userType,
    name,
});

export default connect(mapStateToProps, { })(
    UserSidebar
);
