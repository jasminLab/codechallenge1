import React, { Component } from "react";
import { Menu, Image, Icon, Dropdown, Button } from "semantic-ui-react";
import { connect } from 'react-redux';
import "./Navbar.scss";

const bkTechHouseLogo = require("../../../assets/images/bkTechHouse.jpg");

const trigger = (
	<span>
		<Icon className='bell' size='big' name='user circle' />
	</span>
);

class Navbar extends Component {

	logout = (e) => {
		e.preventDefault();
		localStorage.clear();
		window.location.replace('/');
	};

	render() {
		const { userType, firstName } = this.props;
		return (
			<div className='Navbar'>
				<div className='navbar-menu'>
					<Menu className="border_bottom" secondary>
						<div className='div-left'>
							<Image
								className='logo-lima'
								src={bkTechHouseLogo}
								alt='logo'
							/>
							<div className="text-bottom">
								<p className="text-agriculture">BK-Teck Challenge</p>
							</div>
						</div>

						<Menu.Menu position='right'>
							<Menu.Item>
								<Dropdown trigger={trigger}>
									<Dropdown.Menu>
										<Dropdown.Item>Name : <span style={{ color: '#2e7d32', fontWeight: 'bold' }}>
											{firstName}</span></Dropdown.Item>
										<Dropdown.Item>Role : <span style={{ color: '#2e7d32', fontWeight: 'bold' }}>
											{userType}</span></Dropdown.Item>
										<Dropdown.Divider />
										<Dropdown.Item
											icon='wrench'
											text='Manage your account'
										/>
										<Dropdown.Divider />
										<Menu.Item
											name='logout'
											onClick={this.logout}
											to="/"
										>
											<Button className="btn-logout" primary>Logout</Button>
										</Menu.Item>
									</Dropdown.Menu>
								</Dropdown>
							</Menu.Item>
						</Menu.Menu>
					</Menu>
				</div>
			</div>
		);
	}
}

const mapStateToProps = ({
	user: {
		profile: { firstName, userType }
	}
}) => ({
	firstName, userType
});

export default connect(mapStateToProps, {})(Navbar);