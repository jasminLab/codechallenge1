import _ from 'lodash';
import React, { Component } from 'react';
import { Card, Table, Search, Grid, Button, Icon, Popup, Message, Pagination, Modal, Divider } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import { Link } from "react-router-dom";
import { getProduct } from '../../../actions/product';

import '../../../assets/css/table.scss';

class DisplayProduct extends Component {
    
    state = {
        page: 1,
        itemsPerPage: 15,
        products: [],
        open: false,
        theProduct: {}
    }

    closeConfigShow = (closeOnEscape, closeOnDimmerClick, product) => () => {
        this.setState({
            closeOnEscape,
            closeOnDimmerClick,
            open: true,
            theProduct: { ...product }
        })
    }

    close = () => this.setState({ open: false })

    componentDidMount = () => {
        const { getProduct } = this.props;
        getProduct();
    };

    setPageNum = (event, { activePage }) => {
        this.setState({ page: activePage });
    };

    UNSAFE_componentWillReceiveProps = (nextProps) => {
        this.setState({
            products: nextProps.listOfProduct,
        });

        return this.setState;
    };

    render() {
        const { products, open, closeOnEscape, closeOnDimmerClick, theProduct } = this.state;
        const itemsPerPage = 15;
        const { page } = this.state;
        const totalPages = products && products.length / itemsPerPage;
        const items = products && products.slice(
            (page - 1) * itemsPerPage,
            (page - 1) * itemsPerPage + itemsPerPage
        );
        
        return (
            <>
                <ToastContainer position={toast.POSITION.TOP_RIGHT} />
                <Divider hidden />
                <Divider hidden />
                <Grid>
                    <Grid.Column width={3}>
                    </Grid.Column>
                    <Grid.Column className="style-role" width={10}>
                        <Card.Group className="table-card">
                            <Card fluid>
                                <Card.Content className="header-bg-color-public">Products</Card.Content>
                                <Card.Content>
                                    <Grid>
                                        <Grid.Column floated='left' width={7}>
                                            <Link className="link-color" to="/">
                                                <Button>Back Home</Button>
                                            </Link>
                                        </Grid.Column>
                                    </Grid>
                                {Object.keys(products).length > 0 ?
                                <>
                                    <Table className="table-card" singleLine>
                                        <Table.Header>
                                            <Table.Row>
                                                <Table.HeaderCell>Product Name</Table.HeaderCell>
                                                <Table.HeaderCell>Price</Table.HeaderCell>
                                                <Table.HeaderCell>Manufacture Date</Table.HeaderCell>
                                                <Table.HeaderCell>Status</Table.HeaderCell>
                                                <Table.HeaderCell>Action</Table.HeaderCell>
                                            </Table.Row>
                                        </Table.Header>

                                        <Table.Body>
                                        {_.map(items, (product) => (
                                            <Table.Row key={product.id}>
                                                <Table.Cell>{product.name}</Table.Cell>
                                                <Table.Cell>{product.price}</Table.Cell>
                                                <Table.Cell>{product.manufdate}</Table.Cell>
                                                <Table.Cell>
                                                    <Icon color={(product.status === 'ACTIVE') ? "green" : "red"} name="circle" />
                                                </Table.Cell>
                                                <Table.Cell className="text-center">
                                                    <Popup content='View' trigger={<Icon onClick={this.closeConfigShow(false, true, product)} color="" name="eye" />} />
                                                </Table.Cell>
                                                
                                                {/* Confirm delete */}
                                                <Modal
                                                size=""
                                                open={open}
                                                closeOnEscape={closeOnEscape}
                                                closeOnDimmerClick={closeOnDimmerClick}
                                                onClose={this.close}
                                                >
                                                <Modal.Header>Product Details</Modal.Header>
                                                <Modal.Content>
                                                    <Grid>
                                                        <Grid.Column floated='left'>
                                                        <h4>Product</h4>
                                                        <p>Name: {theProduct.name}</p>
                                                        <p>Price: {theProduct.price}</p>
                                                        <p>Manufacture Date: {theProduct.manufdate}</p>
                                                        <p>Status: {theProduct.status}</p>
                                                        <p>Description: {theProduct.description}</p>
                                                        <p>createddate: {theProduct.createddate}</p>
                                                        <p>updateddate: {theProduct.updateddate}</p>
                                                        </Grid.Column>
                                                    </Grid>
                                                </Modal.Content>
                                                <Modal.Actions>
                                                    <Button onClick={this.close} negative>Close</Button>
                                                </Modal.Actions>
                                                </Modal>
                                            </Table.Row>
                                            ))}
                                        </Table.Body>
                                    </Table>
                                    <div className="center-pagination">
                                        <Pagination
                                            activePage={page}
                                            totalPages={totalPages}
                                            siblingRange={1}
                                            onPageChange={this.setPageNum}
                                        />
                                    </div>
                                    </> : 
                                    <Message info>
                                    <p className="center-text">No Products available, Please create..</p>
                                </Message>
                                }
                                </Card.Content>
                            </Card>
                        </Card.Group>
                    </Grid.Column>
                    <Grid.Column width={3}>
                    </Grid.Column>
                </Grid>
                <p id="footer-content">Copyright &copy; 2020</p>
            </>
        );
    }
}

const mapStateToProps = ({
    product: {
        listOfProduct, listOneProduct,
    } }) => ({
        listOfProduct,
        listOneProduct
 });

export default connect(mapStateToProps, { getProduct })(DisplayProduct);
