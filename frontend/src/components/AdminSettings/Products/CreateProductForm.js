import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { ToastContainer, toast } from 'react-toastify';
import { connect } from 'react-redux';
import { createProduct } from '../../../actions/product';

import Btn from '../../common/Button/Button';
import '../../../assets/css/table.scss';

class CreateProductForm extends Component {

    state = {
        form: {
            name: '',
            price: '',
            description: '',
            manufDate: ''
        },
        errors: {},
        loading: false,
        message: '',
        key: '',
        errorUp: ''
    }

    componentDidMount = () => {
    };

    handleChange = (_, data) => {
        const { form, errors } = this.state;
        this.setState({
            form: { ...form, [data.name]: data.value },
            errors: { ...errors, [data.name]: null },
            loading: false,
            message: ''
        });
    };

    static getDerivedStateFromProps = (props) => {
        return {
            errorUp: props.errorsProduct
        };
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { createProduct } = this.props;
        const { form } = this.state;
        const { ...formData } = form;
        createProduct(formData);
        this.setState({
            form: { name: '', price: '', description: '', manufDate: '' },
            errors: {}
        });
    };

    render() {
        const { loadingProduct } = this.props;
        const { form, errors, errorUp } = this.state;

        return (
            <div>
                <ToastContainer position={toast.POSITION.TOP_RIGHT} />
                
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group widths='equal'>
                        <Form.Input
                            required
                            label="Product Name"
                            placeholder="Product Name"
                            name="name"
                            type="text"
                            onChange={this.handleChange}
                            value={form.name || ""}
                            error={errors.name}
                        />
                        <Form.Input
                            required
                            label="Price"
                            placeholder="Price"
                            name="price"
                            type="text"
                            onChange={this.handleChange}
                            value={form.price || ""}
                            error={errors.price}
                        />
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Input
                            required
                            label="Manufacture Date"
                            placeholder="Manufacture Date"
                            name="manufDate"
                            type="date"
                            onChange={this.handleChange}
                            value={form.manufDate || ""}
                            error={errors.manufDate}
                        />
                        <Form.Input
                            required
                            label="Description"
                            placeholder="Description"
                            name="description"
                            type="text"
                            onChange={this.handleChange}
                            value={form.description || ""}
                            error={errors.description}
                        />
                    </Form.Group>

                    <Btn type="submit" className="" primary loading={loadingProduct}>CREATE</Btn>
                </Form>
                <p id="footer-content">Copyright &copy; 2020</p>
            </div>
        );
    }
}

const mapStateToProps = ({
    user: { profile },
    product: {
        createProduct: {
            loading: loadingProduct,
            message: messageProduct,
            errors: errorsProduct
        }
    }}) => ({
        profile,
        loadingProduct,
        messageProduct,
        errorsProduct
    });

export default connect(mapStateToProps, { createProduct })(CreateProductForm);
