import React, { Component } from "react";
import { Form, Select, Message } from "semantic-ui-react";
import { ToastContainer, toast } from "react-toastify";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { accountTypes } from "../../../helpers//userRegistration/accountType";
import { create } from "../../../actions/user";

import Btn from "../../common/Button/Button";
import "../Register.scss";

class UserFirstDetails extends Component {
  state = {
    form: {
      firstName: "",
      lastName: "",
      email: "",
      userType: "",
      phone: "",
      username: "",
      password: ""
    },
    errors: {},
    loading: false,
    message: "",
    errorUp: {},
  };

  handleChange = (_, data) => {
    const { form, errors } = this.state;
    this.setState({
      form: { ...form, [data.name]: data.value },
      errors: { ...errors, [data.name]: null },
      loading: false,
      message: "",
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.props);
    const { createUser } = this.props;
    const { form } = this.state;
    let { ...formData } = form;

    createUser(formData);
    
    this.setState({
      form: {
        firstName: "",
        lastName: "",
        email: "",
        userType: "",
        phone: "",
        username: "",
        password: ""
      },
      errors: {} });
  };

  static getDerivedStateFromProps = (props) => {
    return {
      errorUp: props.errors,
    };
  };

  render() {
    const { loading } = this.props;
    const { form, errors, errorUp } = this.state;
    return (
      <div>
        <div className="scroll-sign-up" id="style-1">
          {errorUp && Object.values(errorUp).length > 0 ? (
            <Message color="red">{Object.values(errorUp)}</Message>
          ) : (
            ""
          )}
          <ToastContainer position={toast.POSITION.TOP_RIGHT} />
          <Form onSubmit={this.handleSubmit}>
            <Form.Group widths="equal">
              <Form.Input
                required
                label="firstName"
                placeholder="firstName......"
                name="firstName"
                type="text"
                onChange={this.handleChange}
                value={form.firstName || ""}
                error={errors.firstName}
              />
              <Form.Input
                required
                label="lastName"
                placeholder="lastName...."
                name="lastName"
                type="text"
                onChange={this.handleChange}
                value={form.lastName || ""}
                error={errors.lastName}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Input
                required
                label="email"
                placeholder="Email...."
                name="email"
                type="email"
                onChange={this.handleChange}
                value={form.email || ""}
                error={errors.email}
              />
              <Form.Field
                required
                id="select"
                control={Select}
                label="User Role"
                onChange={this.handleChange}
                options={accountTypes}
                placeholder="User Role...."
                name="userType"
                search
                value={form.userType || ""}
                error={errors.userType}
              />
            </Form.Group>
            <Form.Group widths="equal">
              <Form.Input
                required
                label="Phone"
                placeholder="Phone...."
                name="phone"
                type="text"
                onChange={this.handleChange}
                value={form.phone || ""}
                error={errors.phone}
              />
            </Form.Group>

            <Form.Group widths="equal">
            <Form.Input
                required
                label="Username"
                placeholder="Username...."
                name="username"
                type="text"
                onChange={this.handleChange}
                value={form.username || ""}
                error={errors.username}
              />
              <Form.Input
                required
                pattern=".{8,}"
                title="8 characters minimum"
                label="password"
                placeholder="Password...."
                name="password"
                type="password"
                onChange={this.handleChange}
                value={form.password || ""}
                error={errors.password}
              />
            </Form.Group>
            <Btn
              type="submit"
              className="btn-sign-in"
              primary
              loading={loading}
            >
              SIGN UP
            </Btn>
            <Link to="/">
              <p className="forgot-password">Login</p>
            </Link>
          </Form>
        </div>
      </div>
    );
  }
}

UserFirstDetails.propTypes = {
  nextStep: PropTypes.func,
  form: PropTypes.object,
  handleChange: PropTypes.func,
};

const mapStateToProps = ({
  user: {
    createUser: { loading, message, errors },
  },
}) => ({
  loading,
  message,
  errors,
});

const mapDispatchToProps = (dispatch) => ({
  createUser: (payload) => dispatch(create(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserFirstDetails);
