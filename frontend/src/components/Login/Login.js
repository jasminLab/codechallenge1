import React, { Component } from "react";
import { Grid, Image, Form } from "semantic-ui-react";
import { ToastContainer, toast } from "react-toastify";
import { Link, Redirect } from "react-router-dom";
import { login } from "../../actions/user";
import { connect } from "react-redux";
import { validateLima } from "../../helpers/validation";
import Btn from "../common/Button/Button";
import RaindropImage from "../common/Raindrop/Raindrop";

import "./Login.scss";

const customId = "custom-id-yes";

const bkTechHouseLogo = require("../../assets/images/bkTechHouse.jpg");

export class Login extends Component {
  state = {
    form: {
      email: "",
      password: "",
    },
    errors: {},
    loading: false,
    message: "",
    newRole: "",
  };

  static getDerivedStateFromProps(props) {
    const eeee = props && props.errors;
    // const newError = eeee && eeee.error;
    const newError = eeee;
    const { profile } = props;
    const { userType } = profile;
    const result = userType;
    // const result2 =
    //   result &&
    //   result.map((obj) => {
    //     return obj.authority;
    //   });

    if (newError)
      return (
        newError &&
        toast.error(newError, {
          toastId: customId,
        })
      );

    if (result && result === 'adminUser') {
      props.history.push("/product");
    }

    if (result && result === 'sellerUser') {
      props.history.push("/product");
    }

    return {
      newRole: result,
    };
  }

  handleChange = (e) => {
    const { form, errors } = this.state;
    this.setState({
      form: { ...form, [e.target.name]: e.target.value },
      errors: { ...errors, [e.target.name]: null },
      loading: false,
      message: "",
    });
  };

  handeleSubmit = (e) => {
    e.preventDefault();
    const { form, errors } = this.state;
    const { login } = this.props;
    const { ...formData } = form;
    const formErrors = validateLima(form, "loginUser");

    this.setState({ errors: { ...errors, ...formErrors } });

    if (!Object.keys(formErrors).length) {
      login(formData);
    }
  };

  render() {
    const { loading, profile } = this.props;
    const { form, errors } = this.state;
    return (
      <div className="select-part" id="element">
        <ToastContainer position={toast.POSITION.TOP_RIGHT} />
        {!loading && Object.keys(profile).length ? (
          <Redirect to="/product" />
        ) : (
          ""
        )}
        <Grid divided="vertically">
          <Grid.Row columns={2}>
            <Grid.Column>
              <RaindropImage>
                <Link to="/view-products">
                  <Btn id="btn-rain">View Products</Btn>
                </Link>
              </RaindropImage>
            </Grid.Column>
            <Grid.Column>
              <div>
                <Image className="logo-lima" src={bkTechHouseLogo} alt="logo" />
                <p className="text-lima">BK TecHouse is an Innovative Technology Company</p>
                <p className="login-text">LOGIN</p>
                <Grid centered columns={2}>
                  <Grid.Column>
                    <Form onSubmit={this.handeleSubmit}>
                      <Form.Group widths="equal">
                        <Form.Input
                          type="text"
                          placeholder="Email"
                          name="email"
                          label="Enter your Email"
                          type="text"
                          onChange={this.handleChange}
                          value={form.email || ""}
                          error={errors.email}
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Input
                          type="password"
                          placeholder="password"
                          name="password"
                          label="Password"
                          type="password"
                          onChange={this.handleChange}
                          value={form.password || ""}
                          error={errors.password}
                        />
                      </Form.Group>
                      <Btn
                        className="btn-sign-in"
                        type="submit"
                        loading={loading}
                      >
                        SIGN IN
                      </Btn>
                      <Link to="/register">
                        <p className="forgot-password">Register</p>
                      </Link>
                      <p className="copyright">copyright 2020</p>
                    </Form>
                  </Grid.Column>
                </Grid>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = ({
  user: {
    loginUser: { errors, message, loading },
    profile,
  },
}) => ({
  errors,
  message,
  loading,
  profile,
});

export default connect(mapStateToProps, { login })(Login);
