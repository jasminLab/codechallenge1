import { toast } from 'react-toastify';
import { productActionsTypes } from '../../actions-types';

export default (state, { type, payload }) => {
    switch (type) {
        case productActionsTypes.CREATE_PRODUCT_START:
            return {
                ...state,
                createProduct: { ...state.createProduct, message: '', loading: true, errors: {} }
            };
        case productActionsTypes.CREATE_PRODUCT_END:
            return {
                ...state,
                createProduct: { ...state.createProduct, loading: false }
            };
        case productActionsTypes.CREATE_PRODUCT_SUCCESS:
            const { message } = payload.data;
            toast.success(message, {
                toastId: 'custom-id-yes2'
            });
            return {
                ...state,
                createProduct: { loading: false, message: payload, errors: {} }
            };
        case productActionsTypes.CREATE_PRODUCT_FAILURE:
            const anError = payload.message || payload.error;
            toast.error(anError, {
                toastId: 'custom-id-yes2'
            });
            return {
                ...state,
                createProduct: { loading: false, message: '', errors: { message: payload.message || payload.error, ...payload.errors } }
            };
        default:
            return null;
    }
};
