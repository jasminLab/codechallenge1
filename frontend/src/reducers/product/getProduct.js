import { productActionsTypes } from '../../actions-types';

export default (state, { type, payload }) => {
    switch (type) {
        case productActionsTypes.GET_PRODUCT_START:
            return {
                ...state,
                fetchProduct: { ...state.fetchProduct, message: '', loading: true, errors: {} }
            };
        case productActionsTypes.GET_PRODUCT_END:
            return {
                ...state,
                fetchProduct: { ...state.fetchProduct, loading: false }
            };
        case productActionsTypes.GET_PRODUCT_SUCCESS:
            return {
                ...state,
                listOfProduct: [...payload.data],
                fetchProduct: { loading: false, message: payload.message, errors: {} }
            };
        case productActionsTypes.GET_PRODUCT_FAILURE:
            return {
                ...state,
                fetchProduct: { loading: false, message: '', errors: { message: payload.message, ...payload.errors } }
            };
        default:
            return null;
    }
};
