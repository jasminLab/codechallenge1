import { product as initialState } from '../../store/initialState';

import clearProductStoreReducer from './clearProductReducer';
import createProduct from './createProduct';
import getProduct from './getProduct';

export default (state = initialState, action) => {
    const clearProductStore = clearProductStoreReducer(state, action);
    const create = createProduct(state, action);
    const getProducts = getProduct(state, action);

    return (
        clearProductStore
        || create
        || getProducts
        || state
    );
};
