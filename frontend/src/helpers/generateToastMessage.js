import { toast } from "react-toastify";

const customId2 = 'yes11'

export const generateToastMessage = (toastMessage, error) => {
  if(error){
    return toast.error(toastMessage, {
      toastId: customId2
   });
  } else{
    return toast.success(toastMessage, {
      toastId: customId2
   });
  } 
}
