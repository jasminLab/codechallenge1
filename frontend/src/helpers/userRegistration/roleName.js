export const roleName = [
    {
        key: 'admin',
        text: 'adminUser',
        value: 'adminUser',
     },
     {
        key: 'seller',
        text: 'sellerUser',
        value: 'sellerUser',
     },
     {
        key: 'buyer',
        text: 'buyerUser',
        value: 'buyerUser',
     }

];
