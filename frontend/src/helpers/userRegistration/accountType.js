export const accountTypes = [
   {
      key: 'admin',
      text: 'Admin User',
      value: 'adminUser',
   },
   {
      key: 'seller',
      text: 'Seller User',
      value: 'sellerUser',
   },
   {
      key: 'buyer',
      text: 'Buyer User',
      value: 'buyerUser',
   }
];
