import { produce } from 'immer';

export const mapValues = (prop) => {
    return {
        key: prop.id,
        text: prop.name,
        value: prop.id
    }
};

export const mapValuesEdit = (prop) => {
    return {
        key: prop && prop.id,
        text: prop && prop.name,
        value: prop && prop.id
    }
}

export const returnNewArray = (data) => {
    let options = [];
    data.forEach(element => {
        let option = {
            key: null,
            text: '',
            value: ''
        };
    
    const updatedOption = produce(option, draftOption => {
        draftOption.key = element.id;
        draftOption.text = element.name;
        draftOption.value = element.id;
    });

    options.push(updatedOption)
        
    }); 

    return options;
}

export const returnAllMemberOptions = (data) => {
    let options = [];
    data.forEach(element => {
        let option = {
            key: null,
            text: '',
            value: ''
        };
    
    const updatedOption = produce(option, draftOption => {
        draftOption.key = element.id;
        draftOption.text = element.lastName;
        draftOption.value = element.id;
    });

    options.push(updatedOption)
        
    }); 

    return options;
}

export const reaturnAuthorsName = (threads) => {
    let authors = []
    threads.forEach(element => {
    let targert = {
        text: element.authorName,
        value: element.authorName
    }
    const author = authors.find(auth => auth.value === element.authorName)
    if(!author) {
        authors.push(targert);
    }
    });

    return authors
}

export const mapValues_FirsN_LastN_ID = (prop) => {
    return {
        key: prop.id,
        text: `${prop.firstName} ${prop.lastName}`,
        value: prop.id
    }
};