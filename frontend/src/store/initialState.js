const user = require('./initialStates/userInitialState');
const product = require('./initialStates/productInitialState');

module.exports = {
    user,
    product
};
