import createProduct from './createProduct';
import getProduct from './getProduct';

export { createProduct, getProduct }
