import { productActionsTypes } from '../../actions-types';
import { apiAction } from '../../helpers';

export default () => dispatch => dispatch(apiAction({
    method: 'get',
    url: '/api/v1/products',
    onStart: productActionsTypes.GET_PRODUCT_START,
    onEnd: productActionsTypes.GET_PRODUCT_END,
    onSuccess: productActionsTypes.GET_PRODUCT_SUCCESS,
    onFailure: productActionsTypes.GET_PRODUCT_FAILURE
}));
