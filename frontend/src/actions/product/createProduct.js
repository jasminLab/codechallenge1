import { productActionsTypes } from '../../actions-types';
import { apiAction } from '../../helpers';

export default formData => dispatch => dispatch(apiAction({
    method: 'post',
    url: '/api/v1/products',
    data: { ...formData },
    onStart: productActionsTypes.CREATE_PRODUCT_START,
    onEnd: productActionsTypes.CREATE_PRODUCT_END,
    onSuccess: productActionsTypes.CREATE_PRODUCT_SUCCESS,
    onFailure: productActionsTypes.CREATE_PRODUCT_FAILURE
}));
