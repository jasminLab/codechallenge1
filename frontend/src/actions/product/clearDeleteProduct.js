import { productActionsTypes } from '../../actions-types';

export const clearDeleteProduct = payload => dispatch => dispatch({
  type: productActionsTypes.CLEAR_DELETE_PRODUCT_STORE,
  payload
});