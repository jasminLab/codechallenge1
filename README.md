# codeChallenge1

BKTechHouse code challenge 1

## Features
- Any User can sign up
- Any User can sign in
- Logged-in User can create a Product
- Any User can view all Products
- Any User can view a specified product

## Getting Started
To get started with this app you have to follow all instruction below carefully and implement.

## Prerequisites
First all of, Install the softwares on your local machine
- Install `NodeJS` [NodeJs](https://nodejs.org/en/download/)
- Install `Git` [Git](https://git-scm.com/)

## Installing the App
Make sure that you have cloned this Repo to your local machine
- By running `git clone https://gitlab.com/jasminLab/codechallenge1.git`
- or download the Ziped folder on `GitLab`
- Then after run `cd codechallenge1` to open the folder or simplly double on the downloaded folder
- To install all dependencies locally (on Backend), open `backend` folrder and run this command `npm i` or `npm install` in terminal
- To install all dependencies locally (on Frontend), open `frontend` folrder and run this command `npm i` or `npm install` in terminal

### Scripts to use on Backend
- run `npm run dev` to start development

### Scripts to use on Frontend
- run `npm run start` to start development

## API endpoints

**API endpoints with no authentication**
- POST `api/v1/auth/signup` User Sign up

**Body:** {
	"firstName": "xxxxxx",
	"lastName": "xxxxxx",
	"email": "xxxxxx",
	"userType": "xxxxxx",
	"phone": "xxxxxx",
	"username": "xxxxxx",
	"password": "xxxxxx"
}

- POST `api/v1/auth/signin` User Sign in

**Body:** {
	"email": "xxxxxx",
	"password": "xxxxxx"
}


- GET `api/v1/products` View all products
- GET `api/v1/products/:productId` Get a specific product


**API endpoints with authentication**
- POST `api/v1/products` Create a product

**Header:** "token": "xxxxxx"

**Body:** {
	"name": "xxxxxx",
	"price": "xxxxxx",
	"description": "xxxxxx",
	"manufDate": "xxxxxx"
}


## UI Pages

### UI pages for avaible for any user
- Home page for sign-up or sign-in [http://localhost:3000](http://localhost:3000)
- Sign-up or sign-up [http://localhost:3000/register](http://localhost:3000/register)
- List all Products [http://localhost:3000/view-products](http://localhost:3000/view-products)

### UI pages for LoggedIn users:
- Product creation [http://localhost:3000/product](http://localhost:3000/product)

## Tools Used

### Back End
* Node JS
* Express (Framework)

### Front End
* Reactjs (HTML, CSS, Javascript)

## Author
- SHYAKA Jasmin <jajajaden01@gmail.com>
---

## Copyright
Copyright (c) Jasmin SHYAKA, Software Engineer
