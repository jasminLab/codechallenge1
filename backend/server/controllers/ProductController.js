import Product from '../models/Product';
import Helper from '../helpers/theHelper';

class ProductController {
  static async createProduct(req, res) {
    try {
      const userId = req.userSignedIn.id;

      req.body.createdDate = Helper.currentDate();

      const dataExist = await Product.productExit(req.body);
      if (dataExist[0]) return res.status(409).json({ status: res.statusCode, error: 'Sorry! This product already exist.' });

      const isSaved = await Product.saveProduct(req.body, Number(userId));
      return res.status(201).json({
        status: res.statusCode,
        data: {
          product: {
            id: isSaved.id,
            name: isSaved.name,
            price: isSaved.price,
            manufDate: isSaved.manufDate,
            description: isSaved.description,
            status: isSaved.status,
            createdDate: isSaved.createdDate,
            createdBy: isSaved.createdBy,
          },
          message: `Created ${req.body.name} product, Successfuly`,
        },
      });
    } catch (err) {
      return res.status(500).json({
        status: res.statusCode,
        error: err.message,
      });
    }
  }

  static async viewProducts(req, res) {
    try {
      const products = await Product.getProducts();

      if (!products) {
        return res.status(404).json({ status: res.statusCode, message: 'Sorry! there are no Products.' });
      }

      return res.status(200).json({ status: res.statusCode, data: products });
    } catch (err) {
      return res.status(500).json({
        status: res.statusCode,
        error: err.message,
      });
    }
  }

  static async viewOneProduct(req, res) {
    try {
      const oneData = await Product.getProductById(req.params.productId);
      if (!oneData) return res.status(404).json({ status: res.statusCode, error: 'Sorry! That product not found.' });

      return res.status(200).json({ status: res.statusCode, data: oneData });
    } catch (err) {
      return res.status(500).json({
        status: res.statusCode,
        error: err.message,
      });
    }
  }
}

export default ProductController;
