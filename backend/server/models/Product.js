import DBConnection from './DBConnection';
import Queries from './Queries';

class Product {
  static async productExit({ name, description }) {
    await DBConnection.query(Queries.productTable.createTable);

    const { rows } = await DBConnection.query(
      Queries.productTable.productExist, [name, description],
    );

    return rows;
  }

  static async saveProduct({
    name, price, manufDate, description, createdDate,
  }, userId) {
    const { rows } = await DBConnection.query(
      Queries.productTable.insertProduct,
      [name, price, manufDate, description, 'ACTIVE', createdDate, '', userId],
    );
    return rows[0];
  }

  static async getProducts() {
    const { rows } = await DBConnection.query(
      Queries.productTable.allProducts,
    );

    return rows;
  }

  static async getProductById(id) {
    const { rows } = await DBConnection.query(
      Queries.productTable.anProduct, [id],
    );

    return rows[0];
  }

  static async getProductByIdAndUserId(userId, productId) {
    const { rows } = await DBConnection.query(
      Queries.productTable.oneProduct, [userId, productId],
    );

    return rows[0];
  }
}

export default Product;
