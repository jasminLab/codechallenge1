const userTable = {
  createTable: `CREATE TABLE IF NOT EXISTS 
      users(
        id SERIAL PRIMARY KEY NOT NULL,
        firstName VARCHAR(200) NOT NULL,
        lastName VARCHAR(200) NOT NULL,
        email VARCHAR(100) UNIQUE NOT NULL,
        userType VARCHAR(100) NOT NULL,
        phone VARCHAR(50) NOT NULL,
        username VARCHAR(100) NOT NULL,
        password VARCHAR(128) NOT NULL,
        status VARCHAR(50) NOT NULL,
        createdDate VARCHAR(100) NOT NULL,
        updatedDate VARCHAR(100)
        )`,
  insertUser: 'INSERT INTO users( firstName, lastName, email, userType, phone, username, password, status, createdDate, updatedDate) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *',
  isUserExist: 'SELECT * FROM users WHERE email = $1',
};

const productTable = {
  createTable: `CREATE TABLE IF NOT EXISTS 
      products(
        id SERIAL PRIMARY KEY NOT NULL,
        name VARCHAR(150) NOT NULL,
        price VARCHAR(128) NOT NULL,
        manufDate VARCHAR(100) NOT NULL,
        description TEXT NOT NULL,
        status VARCHAR(50) NOT NULL,
        createdDate VARCHAR(100) NOT NULL,
        updatedDate VARCHAR(100),
        createdBy INT NOT NULL
        )`,
  insertProduct: 'INSERT INTO products( name, price, manufDate, description, status, createdDate, updatedDate, createdBy) VALUES($1 ,$2, $3, $4, $5, $6, $7, $8) RETURNING *',
  productExist: 'SELECT * FROM products WHERE name = $1 AND description = $2',
  allProducts: 'SELECT * FROM products ORDER BY id DESC LIMIT 10',
  allUserProducts: 'SELECT * FROM products WHERE createdBy = $1 ORDER BY id DESC LIMIT 10',
  oneProduct: 'SELECT * FROM products WHERE createdBy = $1 AND id = $2',
  anProduct: 'SELECT * FROM products WHERE id = $1',
};

export default { userTable, productTable };
