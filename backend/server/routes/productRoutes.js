import express from 'express';
import ProductController from '../controllers/ProductController';
import Validations from '../middleware/Validations';
import HeaderToken from '../middleware/HeaderToken';

const router = express.Router();

router.post('/products', Validations.validateProduct, HeaderToken.isUser, ProductController.createProduct);
router.get('/products', ProductController.viewProducts);
router.get('/products/:productId', ProductController.viewOneProduct);

export default router;
